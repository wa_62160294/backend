const mongoose = require('mongoose')
const { Schema } = mongoose
const EventSchema = Schema({
  title: String,
  content: String,
  class: String,
  startDate: Date,
  endDate: Date
},
{
  timestamps: true
})

module.exports = mongoose.model('Event', EventSchema)
